export const imgerror = {
  // 写各种钩子,去影响使用这个指令的那个 dom
  // 各种钩子可以查询文档, 现在用一个 inserted
  // 使用这个指令的元素被插入父节点时自动触发的函数

  inserted(dom, options) {
    // 没有图片也把地址改为默认地址
    if (!dom.src) {
      dom.src = options.value
    }
    // console.log('图片错误了')
    dom.onerror = function() {
      dom.src = options.value
    }
  },
  // 需要调用拎一个钩子函数
  // 当该节点的父组件发生变化的时候，inserted并不会重新触发
  componentUpdated(dom, options) {
    if (!dom.src) {
      dom.src = options.value
    }
  }

}
