import Vue from 'vue'// 引入Vue
import VueI18n from 'vue-i18n'// 引入国际化的包
import Cookie from 'js-cookie'// 引入cookie包
import elementEn from 'element-ui/lib/locale/lang/en' // 引入饿了么的英文包
import elementZH from 'element-ui/lib/locale/lang/zh-CN'// 引入饿了么的中文包

// 引入自定义语言包
import LangZh from './zh'
import LangEn from './en'
Vue.use(VueI18n)// 全局注册国际化包
export default new VueI18n({
  // 配置1, 当前语言设定
  locale: Cookie.get('language') || 'zh', // 从cookie中获取语言类型 获取不到就是中文
  // 配置2, 字典
  messages: {
    en: {
      ...elementEn, // 将饿了么的英文语言包引入
      hi: 'hi',
      greeting: 'Have a nice day!',
      ...LangEn
    },
    zh: {
      ...elementZH, // 将饿了么的中文语言包引入
      hi: '你好',
      greeting: '祝你开心每一天！',
      ...LangZh
    }
  }
})
