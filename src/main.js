import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n
import i18n from '@/lang'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

// 测试自定义指令
// Vue.directive(名字, 配置对象)
// 使用 Vue.component(组件名称, 组件的对象)
import { imgerror } from '@/directives'
import PageTools from '@/components/PageTools'
import UploadExcel from '@/components/UploadExcel'
import ImageUpload from '@/components/ImageUpload'
import ScreenFull from '@/components/ScreenFull'
import ThemePicker from '@/components/ThemePicker'
import Lang from '@/components/Lang'
// 注册全局组件
Vue.component('PageTools', PageTools)
Vue.component('UploadExcel', UploadExcel)
Vue.component('ImageUpload', ImageUpload)
Vue.component('ScreenFull', ScreenFull)
Vue.component('ThemePicker', ThemePicker)
Vue.component('Lang', Lang)
Vue.directive('imgerror', imgerror)

// // 注册全局过滤器
// // Vue.filter(过滤器名, 过滤器函数)
// Vue.filter('formatDate', (oldValue) => {
//   console.log(oldValue)
//   //  最简单的格式化方法,
//   // 1.转换为日期对象
//   const date = new Date(oldValue)
//   // 2.调用 toLocaleString 方法转换成用户浏览器的对应设置格式
//   return date.toLocaleDateString()
// })
// 以下演示大量过滤器同时注册的封装方法
// import { formatDate } from '@/filters'
// Vue.filter('formatDate', formatDate)

// 2. 演示一次将一个文件里面的所有方法全部引入的语法
import * as filters from '@/filters'
// 这里我们用更加简单的用法 forin 进行遍历
for (const key in filters) {
  // key 就是过滤器名, filters[key] 就是过滤器函数体
  Vue.filter(key, filters[key])
}
// set ElementUI lang to EN
// Vue.use(ElementUI,{ locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})

Vue.config.productionTip = false

// 全局注册打印插件
import Print from 'vue-print-nb'
Vue.use(Print)

// Mixin 混入的语法
// Vue.mixin({
//   // 这个对象可以写 vue 组件内支持的任意内容
//   // 这里写的内容, 会被自动混入到所有的 vue 组件当中
//   methods: {
//     showTen() {
//       console.log(10)
//     }
//   }
// })

import checkPermission from '@/mixin/checkPermission'

Vue.mixin(checkPermission)

new Vue({
  el: '#app',
  router,
  store,
  // 4. 挂载到 vue 上面
  i18n,
  render: h => h(App)
})
