// 引入router
import router from '@/router'
import store from '@/store'
// 引入一份进度条插件
import NProgress from 'nprogress'
// 引入进度条样式
import 'nprogress/nprogress.css'

// 导航守卫
router.beforeEach(async(to, from, next) => {
  // 进度条开始
  NProgress.start()
  // 创建白名单，排除不需登录的页面，防止死循环
  const whiteList = ['/login', '/404']
  // // to 目标 from 来源 next 放行

  // 判断有无token
  if (store.getters.token) {
    // 如果有token
    // 判断是否跳转登录页
    if (to.path === '/login') {
      // 不给他去登录页
      next('/')
    } else {
      // 其他页面放过
      // 这里全部都是后台的管理页面
      // 按道理全部都应该获得用户信息
      // 但每一页都自己写太烦人
      // /在这里，进入页面之前，先调用，保证所有的页面都有用户信息
      // this.$store.dsipatch('user/getUserInfo')
      // 这个获取用户数据是一个异步请求，所以不能直接往下走，必须等他完成
      // 另外，每页都获取，其实可能浪费，因为这个获取是放在公共的vuex里面的
      // 如果前面的页面已经获取过，下一个跳转的页面就没有必要再获取了，做一个判断
      if (!store.getters.name) {
        const res = await store.dispatch('user/getUserInfo')
        // 以上是路由守卫获取用户信息的地方
        // 本来获取完就放行, 但是现在还需要根据这个用户信息里面的权限数据
        // 进行路由筛选
        // console.log('这里是路由守卫 ')
        // console.log('除了调用获取用户信息的方法')
        // console.log('顺便把用户信息拿回来')
        // console.log(res)
        const routes = await store.dispatch('permission/filterRoutes', res.roles.menus)
        console.log(routes)
        // 以上调用的时候, actions 内部会通过mutations改变state从而渲染菜单
        // 但是路由本身的设置并不是直接改数组这么简单, 需要一个方法叫做 addRoutes
        // 所以这个地方出了渲染菜单没问题以外, 还得拿到筛选后的有权限列表, 进行添加
        router.addRoutes([...routes, { path: '*', redirect: '/404', hidden: true }])
        // 固定写法
        // 因为 addRoutes 函数调用时间已经在匹配路由之后
        // 不能马上继续 next 需要原地跳一圈
        // 将 to.path 地址作为目标作为 next 参数
        next(to.path)
      } else {
        next()
      }
    }
  } else {
    // 没有token，判断是否在白名单中
    if (whiteList.indexOf(to.path) >= 0) {
      next()
    } else {
      // 没token没在白名单，跳转登录页
      next('/login')
    }
  }
  // 手动关闭一次, 为了解决 手动切换地址时  进度条的不关闭的问题
  NProgress.done()
})

router.afterEach(route => {
  // 结束进度条
  NProgress.done()
})
