import { asyncRoutes, constantRoutes } from '@/router'
const state = {
  routes: []
}
const mutations = {
  setRoutes(state, asyncRoutes) {
    // 我希望在这里封装一个 muataions
    // 专门将静态路由, 和有权限的动态路由合并
    // 交给 state 储存
    // 用来渲染菜单, 而不是原来的所有菜单都显示出来
    const newRoutes = [...constantRoutes, ...asyncRoutes]
    state.routes = newRoutes
  }
}
const actions = {
  // 封装一个筛选可用路由的方法
  filterRoutes(store, menus) {
    // 参数1 store 本身(context)
    // 参数2 外部调用时传入的数据,这里指用户拥有的 menus 权限数组

    // 我们还需要引入动态路由的配置, 得到所有需要筛选的页面
    // 过滤动态路由, 如果名字存在于 menus 当中则保留, 否则抛弃
    const routes = asyncRoutes.filter(route => menus.indexOf(route.name) > -1)
    // 遍历当前这个人所拥有的 Menus 权限, 如果一个路由name 存在于 menus 当中
    // 就应该通过, 如果不存在则扔掉
    console.log(routes)
    // 解决菜单问题, 将筛选过后的动态路由交给 mutations
    store.commit('setRoutes', routes)
    return routes
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
