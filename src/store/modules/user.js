import { login, getUserInfo, getUserDetailById } from '@/api/user'
import { getToken, setToken, removeToken, setTimeStamp } from '@/utils/auth'
import { resetRouter } from '@/router'
export default {
  namespaced: true,
  state: {
    // 获取历史数据token，没有则显示underfunded
    token: getToken(),
    userInfo: {}
  },
  mutations: {
    stateToken(state, data) {
      state.token = data
      // 登录成功将token存入历史数据
      setToken(data)
    },
    // 退出登录，清理数据
    removeToken(state) {
      state.token = ''
      // 退出登录将token删除
      removeToken()
    },
    // 设置用户数据
    setUserInfo(state, data) {
      state.userInfo = data
    },
    // 清除用户数据
    removeUserInfo(state) {
      state.userInfo = {}
    }
  },
  actions: {
    async login(store, data) {
      try {
        //  发起登录请求
        const res = await login(data)
        // 提交到当前mutations
        // 提交到当前模块可以不用属性名user/stateToken
        store.commit('stateToken', res)
        // 除了记录token 还得记录时间
        setTimeStamp()
      } catch (error) {
        console.log('失败逻辑')
        console.log(error)
      }
    },
    async getUserInfo(store) {
      const resSimple = await getUserInfo()
      // id
      const userId = resSimple.userId
      // 获取用户的详细信息
      const resDetail = await getUserDetailById(userId)
      const res = {
        ...resSimple,
        ...resDetail
      }
      store.commit('setUserInfo', res)
      // 为了之后的操作, 返回这个 res
      console.log(1)
      return res
    },
    // 退出登录
    logout(store) {
      // 清理 state
      // 1. 清理 token
      store.commit('removeToken')
      // 2. 清理 userInfo
      store.commit('removeUserInfo')
      // 3.清理路由信息
      resetRouter()
      // 4. 如果是为了保险起见, 连菜单数据也一并清空
      // 问题在于 permission 并不是当前 user 的子模块
      // 这个mutation 应该基于 最外层的根 vuex 实例来操作
      // 可以通过添加 {root: true} 来制定操作的实例时根vuex 实例
      store.commit('permission/setRoutes', [], { root: true })
    }
  }
}
