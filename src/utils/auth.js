import Cookies from 'js-cookie'

const TokenKey = 'token'
const Time = 'time'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
// 设置时间戳
export function setTimeStamp() {
  Cookies.set(Time, Date.now())
}
// 从Cookies获取时间戳
export function getTimeStamp() {
  return Cookies.get(Time)
}
